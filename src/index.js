import Vue from 'vue'
import List from './components/List.vue'

new Vue({
   render: h => h(List)
 }).$mount('#app')